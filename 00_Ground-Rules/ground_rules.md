
# Ground Rules

### Why this repo?

- Keep the processes we went through in one place so others do not need to waste time searching
- Version controlled so that we can change and update as technology/process evolves
- Built on ground rules so that finding things are easier and (hopefully) does not require rocket science 

### Rules 

- Topics should be filed under individual folders
Example - For setting up Windows Subsystem for Linux (WSL), create a folder

- Folder name should be "<Incremental_Number>_<Topic>".
Example - If WSL is first folder, naming will be "001_WSL"

- For Topic name with multiple words, use hyphen ("-") between worlds
Example - For topic name "WSL Setup", naming will be "001_WSL-Setup"

- Within folder add markdown(.md extension) files. 

- For mutliple files, use sub-numbering (001.1_WSL_Setup). If single file, no need to bother

- Within markdown, use H1(#) for headline

- Use H3 (### ** ) with bold weight for Author/Collator title

- Use H3 (###) for sub-section headers

- Code blocks are recommended but not mandatory



