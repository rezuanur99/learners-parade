
# **WSL2 on Windows 11**
## Collated by - Saif Kabir Asif

### Enable WSL and Virtual Machine Platform (Requires Admin privilege )

```PowerShell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

### WSL Install

wsl --install -d <distroName>

### Export Distro


mkdir D:\backup

wsl --export Ubuntu D:\backup\ubuntu.tar

### Unregister the same distribution to remove it from the C: drive:

wsl --unregister Ubuntu

### Re-Import Ubuntu

mkdir D:\wsl
wsl --import Ubuntu D:\wsl\ D:\backup\ubuntu.tar

By default Ubuntu will use root as the default user, to switch back to previous user

### Change default user

cd %userprofile%\AppData\Local\Microsoft\WindowsApps
ubuntu config --default-user <username
