# **Run Docker in Windows 11 using WSL**  

### (Make sure WSL is enabled and Ubuntu & Docker is installed)

### Start WSL
```
wsl -d Ubuntu-22.04
```
### Mount E drive (Can be any other drive)
```
sudo mount -t drvfs 'E:' /mnt/e
```
### Check for docker image
```
docker images
```
You will see the list of docker images with image id. 

### Mount docker container on a specific folder
```
docker container run -v <local directory>:/home -it --rm <image id> bash
```
**Example:** 
```
docker container run -v /mnt/e/Workbook/:/home -it --rm 174c8c134b2a bash
```
### See Running & Closed Containers
```
docker ps -a
```
### Save Container as Image
```
docker commit <container_id> <new_image_name>
```
### Remove Container 
```
docker rm <container id>
```
### Remove All Container 
```
docker container prune
```
